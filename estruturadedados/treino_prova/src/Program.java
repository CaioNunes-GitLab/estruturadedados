public class Program {
    public static void main(String[] args) {
        ListaSimples ls = new ListaSimples();

        ls.inserirViaIndice(0,"A");
        System.out.println(ls);

        ls.inserirViaIndice(1,"B");
        System.out.println(ls);

        ls.inserirViaIndice(3,"C");
        System.out.println(ls);

        ls.inserirViaIndice(2,"D");
        System.out.println(ls);

        ls.excluirViaIndice(0);
        System.out.println(ls);
    }
}
