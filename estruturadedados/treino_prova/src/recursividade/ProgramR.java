package recursividade;

public class ProgramR {
    public static void main(String[] args) {
        System.out.println(fatorial(5));
        System.out.println(calcularPotencia(2,2));
        System.out.println(somarNumAnteriores(6));
        System.out.println(mdc(2,4));
        System.out.println(mmc(12,9));
    }
    public static int fatorial(int num){
        if(num == 1){
            return 1;
        }
        return num * fatorial(num - 1);
    }

    public static int fibonacci(int qtdNum){
        if(qtdNum == 1 || qtdNum == 2){
            return 1;
        }
        if(qtdNum == 0){
            return 0;
        }
        return fibonacci(qtdNum-1) + fibonacci(qtdNum - 2);
    }

    public static int calcularPotencia(int num, int potencia){
        if(potencia < 1){
            return 1;
        }
        return num * calcularPotencia(num, potencia - 1);
    }

    public static int somarNumAnteriores(int num){
        if(num < 1){
            return 0;
        }
        return num + somarNumAnteriores(num - 1);
    }
    public static int mdc(int num1, int num2){
        if (num1 % num2 == 0){
            return num2;
        }
        return mdc(num2, num1 % num2);
    }

    public static int mmc(int num1, int num2){
        return num1 * (num2 / mdc(num1,num2));
    }
}
