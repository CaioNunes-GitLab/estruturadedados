package inicio;

public class ListaSimples {
    No inicio = null;
    int tamanho = 0;

    public void inserirInicio(String info) {
        No no = new No();
        no.info = info;
        no.proximo = inicio;
        inicio = no;
        tamanho++;
        //quando sair do escopo do método esse nó já irá 'morrer'
    }

    public void inserirFim(String info) {
        No no = new No();
        no.info = info;
        if (inicio == null) {
            inicio = no;
            no.proximo = null;
        } else {
            No local = inicio;
            while (local.proximo != null) {
                local = local.proximo;
            }
            local.proximo = no;
            no.proximo = null;
        }
        tamanho++;
    }

    public String excluirInicio() {
        if (inicio == null) {
            return null;
        }
        String info;
        info = inicio.info;
        inicio = inicio.proximo;
        tamanho--;
        return info;
    }

    public String excluirFim() {
        No local = inicio;

        while (local.proximo.proximo != null) {
              local = local.proximo;
        }
        String info = local.info;
        local.proximo = null;
        tamanho--;
        return info;
    }

//    public String removerPosição(int posicao){
//        if(posicao < 0 || posicao >= tamanho || inicio == null){
//            return null;
//        } else if (posicao == 0) {
//            return excluirInicio();
//        }else if (posicao == tamanho - 1) {
//            return excluirFim();
//        }
//        inicio.No local = inicio;
//        for (int i = 0;i < posicao - 1; i++){
//            local = local.proximo;
//        }
//
//        String info = local.proximo.info;
//        local.proximo = local.proximo.proximo;
//        tamanho--;
//        return info;
//    }

    public String removerInicioAula() {
        if (inicio == null) {
            return null;
        }
        String info;
        info = inicio.info;
        inicio = inicio.proximo;
        tamanho--;
        return "O valor removido da lista foi: " + info;
    }

    public void inserirFimAula(String info) {
        No no = new No();
        no.info = info;
        if (inicio == null) {
            no.proximo = null;
            inicio = no;
        } else {
            No local = inicio;
            while (local.proximo != null) {
                local = local.proximo;
            }
            local.proximo = no;
            no.proximo = null;
        }
        tamanho++;
    }

    public String excluirFimAula() {
        if (inicio == null) {
            return null;
        }
        No local = inicio;
        while (local.proximo != null) {
            No aux = local;
            local = local.proximo;
            if(local.proximo == null){
                aux.proximo = null;
                tamanho--;
                return local.info;
            }
        }
        inicio = null;
        tamanho--;
        return local.info;
    }

    public void inserirViaIndice(int indice, String info){
        if(indice <= 0){
            inserirInicio(info);
        } else if (indice >= tamanho) {
            inserirFim(info);
        } else{
            No local = inicio;
            for (int i = 0; i < indice - 1; i++) {
                local = local.proximo;
            }
            No no = new No();
            no.info = info;
            no.proximo = local.proximo;
            local.proximo = no;
            tamanho++;
        }
    }

    public String excluirViaIndice(int indice){
        if(indice < 0 || inicio == null || indice >= tamanho){
            return null;
        } else if (indice == 0){
            return excluirInicio();
        } else if (indice == tamanho - 1){ //O indice começa a contagem do 0, enquanto o tamanho reflete a quantidade total de elementos da lista
            return excluirFimAula();
        } else {
            No local = inicio;
            for (int i = 0; i < indice - 1; i++){ //A partir do anterior exclui o desejado
                local = local.proximo;
            }
            String info = local.proximo.info;
            local.proximo = local.proximo.proximo;
            tamanho--;
            return info;
        }
    }

    public String toString() {
        String str = "(" + tamanho + ") ";
        No local = inicio;
        while (local != null) {
            str += local.info + " ";
            local = local.proximo;
        }
        return str;
    }
}

