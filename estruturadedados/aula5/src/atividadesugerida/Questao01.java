package atividadesugerida;

public class Questao01<T> {
    No inicio = null;
    int tamanho = 0;

    /*1 – Crie um programa em java que tenha uma lista com os nomes dos
    professores do curso de Análise da UCSAL, contemplando as seguintes
    operações:
    a) Adicionar - OK
    b) Remover - OK
    c) Verificar tamanho - OK
    d) Verificar se está vazia, retornando true - OK
    e) Apagar lista, deletando todos os registros - OK
    f) Listar os registros da lista. - OK */

    public void adicionarElementoViaIndice(String info, int indice){
        if(inicio == null){
            No no = new No();
            no.info = info;
            no.proximo = inicio;
            inicio = no;
            tamanho++;
        } else if (indice >= tamanho) {
            if(inicio == null){
                No no = new No();
                no.info = info;
                no.proximo = inicio;
                inicio = null;
                tamanho++;
            }else{
                No no = new No();
                no.info = info;
                No local = inicio;
                while(local.proximo != null){
                    local = local.proximo;
                }
                local.proximo = no;
                no.proximo = null;
                tamanho++;
            }
        } else{
            No no = new No();
            no.info = info;
            No local = inicio;
            for (int i = 0; i < indice--; i++) {
                local = local.proximo;
            }
            no.proximo = local.proximo;
            local.proximo = no;
            tamanho++;
        }

    }

    public String removerElementoViaIndice(int indice){
        if(inicio == null || indice >= tamanho || indice < 0){
            return null;
        } else if (indice == tamanho-1) {
            No local = inicio;
            String info = inicio.info;
            while (local.proximo.proximo != null) {
                local = local.proximo;
            }
            local.proximo = null;
            tamanho--;
            return info;
        }else if(indice == 0){
            String info;
            info = inicio.info;
            inicio = inicio.proximo;
            tamanho--;
            return info;
        } else{
            No local = inicio;
            int i=0;
            String info = null;
            while (i<=indice) {
                if(i+1==indice) {
                    info = local.proximo.info;
                    local.proximo = local.proximo.proximo;
                    break;
                }
                local = local.proximo;
                i++;
            }
            tamanho--;
            return info;
        }
    }

    public int verificarTamanho () {
        return tamanho;
    }

    public boolean isEmpty(){
        if (inicio == null){
            return true;
        }else {
            return false;
        }
    }

    public String apagarTodos(){
        inicio = null;
        return "Todos os itens da lista foram excluídos";
    }

    public String toString() {
        String str = "(" + tamanho + ") ";
        No local = inicio;
        while (local != null) {
            str += local.info + " , ";
            local = local.proximo;
        }
        return str;
    }
}
