package atividadesugerida;

public class Program {
    public static void main(String[] args) {
        //Adicionar
        Questao01 q = new Questao01();
        q.adicionarElementoViaIndice("a", 0);
        System.out.println(q);
        q.adicionarElementoViaIndice("b", 1);
        System.out.println(q);
        q.adicionarElementoViaIndice("c", 2);
        System.out.println(q);
        q.adicionarElementoViaIndice("d", 3);
        System.out.println(q);

        //Remover
        System.out.println(q.removerElementoViaIndice(1));
        System.out.println(q);

        //É vazio?
        System.out.println(q.isEmpty());

        //Listar
        System.out.println(q.verificarTamanho());

        //DeleteAll
        System.out.println(q.apagarTodos());
        System.out.println(q);




    }
}
