import domain.Sacola;

public class Program {
    public static void main(String[] args) {
        Sacola sacola = new Sacola(10);

        sacola.insere(1); //Arroz
        sacola.insere(3); //Feijão
        sacola.insere(5); //Macarrão
        System.out.println(sacola.tamanho());
        System.out.println(sacola.get(0));

        System.out.println(sacola.max(sacola));
    }
}
