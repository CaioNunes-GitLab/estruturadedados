package interfaces;

public interface SacolaInterface {
    void insere(float o);
    float get(int i);
    int tamanho();
}
