package domain;

import interfaces.SacolaInterface;

public class Sacola implements SacolaInterface {
    private float [] vetor;
    private int n;

    public Sacola(int tamanhoTotal){
        vetor = new float[tamanhoTotal];
        n=0;
    }
    @Override
    public void insere(float item) {
        vetor[n++] = item;
    }

    @Override
    public float get(int i) {
        return vetor[i];
    }

    @Override
    public int tamanho() {
        return n;
    }

    public float max(Sacola s){
    // Acha o maior elemento de uma sacola
        float max = Float.NEGATIVE_INFINITY;
        int n = s.tamanho();
        for (int i=0; i < n; ++i) {
            float item = s.get(i);
            if (item > max)
                max = item;
        }
        return max;
    }
}
