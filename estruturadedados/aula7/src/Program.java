import domain.ContaBancaria;

public class Program {
    public static void main(String[] args) {
        System.out.println(Somar.somando(5));
        System.out.println(Somar.calculaMDC(12,18));

        ContaBancaria cb = new ContaBancaria(100d);
        cb.sacar(10);
        cb.depositar(10);
        System.out.println(cb);
    }
}
