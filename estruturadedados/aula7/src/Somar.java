public class Somar {
    public static int somando(int limite) {
        if (limite == 0) {
            return 0;
        }
        return limite + somando(limite - 1);
    }

    public static int calculaMDC(int num1, int num2) {
        int r;
        if (num1 % num2 == 0) {
            return num2;
        }
        return calculaMDC(num2,(num1 % num2));
    }
}
