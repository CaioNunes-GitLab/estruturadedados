package exercicio;

public class Exercicio<T> {
    private T[] elementos;
    private int topo;


    public Exercicio(int tamanho) {
        this.topo = -1;
    }
    public boolean temCoisaAiNao(){
        return topo == -1;
    }
    public boolean vaiExplodir(){
        return elementos.length == topo + 1;
    }
    public int getQuantidade(){
        return topo + 1;
    }
    public void push(T novo) throws Exception{
        if (vaiExplodir())
            throw new Exception("Expludiu");
        this.elementos[++this.topo] = novo;
    }
    public T pop() throws Exception{
        if (this.temCoisaAiNao()) throw new Exception();
        return this.elementos[this.topo--];
    }
}
