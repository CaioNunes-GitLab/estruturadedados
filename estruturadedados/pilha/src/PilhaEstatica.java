public class PilhaEstatica<T> {

    private T[] elementos;
    private int tops;

//    public T[] getElementos() {
//        return elementos;
//    }
//
//    public int getTops() {
//        return tops;
//    }
    public PilhaEstatica(T[] elementos, int tamanho) {
        this.elementos = (T[]) new Object[tamanho];
        this.tops = -1;
    }
    public boolean temCoisaAiNao(){
        return tops == -1;
    }
    public boolean vaiExplodir(){
        return elementos.length == tops + 1;
    }
    public int getQuantidade(){
        return tops + 1;
    }
    public void push(T novo) throws Exception{
        if (vaiExplodir())
            throw new Exception("Expludiu");
        this.elementos[++this.tops] = novo;
    }



}
