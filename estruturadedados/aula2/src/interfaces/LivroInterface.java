package interfaces;

public interface LivroInterface {

    void insere(String titulo,String editora,int anoPublicado);

    void imprimir();

}
