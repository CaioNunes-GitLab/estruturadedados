package interfaces;

public interface ProfessorInterface {

    void insere(String nome,String codigoDisciplina);
    String imprimir(String nome,String codigoDisciplina);

}
