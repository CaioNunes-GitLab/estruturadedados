public class CalcularFatorial {
    public static void main(String[] args) {
        //System.out.println(fatorial(2));
        //System.out.println(Fibonacci(10));
        System.out.println(Potencia(3,2));
    }
    public static int fatorial(int numero) {
        if (numero == 1){
            return 1;
        }
        //fatorial(numero*(numero-1));
        return numero * fatorial(numero - 1);
    }

    public static int Fibonacci(int quantidadeTermos){
        if(quantidadeTermos == 1 || quantidadeTermos == 0){
            return quantidadeTermos;
        }
        return Fibonacci(quantidadeTermos - 1) + Fibonacci(quantidadeTermos - 2);
    }

    public static int Potencia(int num, int potencia){
        if (potencia < 1 ){
            return 1;
        }
        return num * Potencia(num, potencia - 1);
    }
}


