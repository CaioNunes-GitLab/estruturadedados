package domain;

import interfaces.LivroInterface;

public class Livro implements LivroInterface {
    private String[] titulo = new String[10];
    private String[] editora = new String[10];
    private int[] anoPublicado = new int[10];

    private int indiceVetorTitulo = 0;
    private int indiceVetorEditora = 0;
    private int indiceVetorAno = 0;

    @Override
    public void insere(String titulo, String editora, int anoPublicado) {
        this.titulo[indiceVetorTitulo] = titulo;
        this.editora[indiceVetorEditora] = editora;
        this.anoPublicado[indiceVetorAno] = anoPublicado;
    }
    @Override
    public void imprimir() {
        System.out.println("Título: "+ titulo[indiceVetorTitulo] + "\n"
                +"Editora: " + editora[indiceVetorEditora] + "\n"
                +"Ano Publicado: " + anoPublicado[indiceVetorAno]);
    }
}
