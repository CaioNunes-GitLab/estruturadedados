public class ListaDuplaAula {

    No inicio;
    No fim;
    int tamanho = 0;


    public void inserirInicio(String info){
        No no = new No();
        no.info = info;
        no.anterior = null;
        no.proximo = inicio;
        if(inicio != null){
            inicio.anterior = no;
        }
        inicio = no;
        if(tamanho == 0){
            fim = inicio;
        }
        tamanho++;
    }

    public String excluirInicio(){
        if(inicio == null){
            return null;
        }
        String info = inicio.info;
        inicio = inicio.proximo;
        if(inicio != null){
            inicio.anterior = null;
        }else{
            fim = null;
        }
        tamanho--;
        return info;
    }

    public void inserirFim(String info){
        No no = new No();
        no.info = info;
        no.proximo = null;
        no.anterior = fim;
        if(fim != null){
            fim.proximo = no;
        }
        fim = no;
        if(tamanho == 0){
            inicio = null;
            fim = null;
        }
        tamanho++;
    }



    @Override
    public String toString() {
        String str = "(" + tamanho + ") ";
        No local = inicio;
        while (local != null) {
            str += local.info + " ";
            local = local.proximo;
        }
        return str;
    }

    public String imprimirOrdemInversa() {
        String str = "(" + tamanho + ") ";
        No local = fim;
        while (local != null) {
            str += local.info + " ";
            local = local.anterior;
        }
        return str;
    }
}
