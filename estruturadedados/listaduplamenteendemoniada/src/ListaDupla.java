public class ListaDupla {
    No inicio;
    No fim;
    int tamanho;

    public void inserirInicio(String info) {
        No no = new No();
        no.info = info;
        no.anterior = null;
        no.proximo = inicio;

        if (inicio != null) {
            no.anterior = inicio;
        }
        inicio = no;
        if (tamanho == 0) {
            fim = inicio;
        }

        tamanho++;
    }

    public String removerInicio() {
        if (inicio == null) {
            return null;
        }

        String info = inicio.info;

        inicio = inicio.proximo;
        if (inicio != null) {
            inicio.anterior = null;
        } else {
            fim = null;
        }

        tamanho--;
        return info;
    }

    public void inserirFim(String info) {
        No no = new No();
        no.info = info;
        no.proximo = null;
        no.anterior = fim;

        if (fim != null) {
            fim.proximo = no;
        }
        fim = no;
        if (tamanho == 0) {
            inicio = fim;
        }
        tamanho++;
    }

    public String excluirFim() {
        if (inicio == null) {
            return null;
        }
        String info = fim.info;
        fim = fim.anterior;
        if (fim != null) {
            fim.proximo = null;
        }else {
            inicio = null;
        }
        tamanho--;
        return info;
    }

    public void inserirMeio(int indice,String info){
        if(indice <= 0){
            inserirInicio(info);
        } else if (indice >= tamanho) {
            inserirFim(info);
        } else {
            No local = inicio;
            for (int i = 0; i < indice - 1; i++ ){
                local = local.proximo;
            }
            No no = new No();
            no.info = info;

            no.anterior = local;
            no.proximo = local.proximo;
            local.proximo = no;
            no.proximo.anterior = no;
            tamanho++;
        }

    }

    public String excluirMeio(int indice){
        if(indice < 0 || indice >= tamanho || inicio == null){
            return null;
        } else if (indice == 0) {
            return removerInicio();
        } else if (indice == tamanho - 1) {
            return excluirFim();
        }
        No local = inicio;
        for (int i = 0; i < indice ; i++ ){
            local = local.proximo;
        }

        local.proximo.anterior = local.anterior;
        local.anterior.proximo = local.proximo;

        /*local.proximo = null; FIXME Redundancia, pois apenas o local estar� apontando para ele, ou seja,
                                 assim que sair do escopo do método ele morre... 'F'
        local.anterior = null;*/

        tamanho--;
        return local.info;
    }

    @Override
    public String toString(){
        String str = "";
        No local = inicio;
        while(local != null){
            str += local.info + " ";
            local = local.proximo;
        }
        return str;
    }

    public String ordenacaoReversa(){
        String str = "";
        No local = fim;
        while(local != null){
            str += local.info + " ";
            local = local.anterior;
        }
        return str;
    }
}
